use serde::Deserialize;

#[allow(dead_code)]
#[derive(Debug, Deserialize)]
pub struct License {
	name: String,
	url: String,
}

#[allow(dead_code)]
#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Phonetic {
	pub audio: Option<String>,
	pub license: Option<License>,
	pub source_url: Option<String>,
	pub text: Option<String>,
}

#[derive(Debug, Deserialize)]
pub struct Definition {
	pub definition: String,
	pub synonyms: Vec<String>,
	pub antonyms: Vec<String>,
	pub example: Option<String>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Meaning {
	pub part_of_speech: String,
	pub definitions: Vec<Definition>,
	pub synonyms: Vec<String>,
	pub antonyms: Vec<String>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Word {
	pub word: String,
	pub phonetics: Vec<Phonetic>,
	pub meanings: Vec<Meaning>,
	pub license: License,
	pub source_urls: Vec<String>,
}

#[cfg(test)]
mod tests {
	use crate::dictionary::Word;

	#[test]
	fn parse_example() {
		let word: Result<Vec<Word>, serde_json::Error> = serde_json::from_str(EXAMPLE_WORD);

		assert!(word.is_ok())
	}

	#[test]
	fn return_word() {
		let word: Vec<Word> = serde_json::from_str(EXAMPLE_WORD).unwrap();

		assert_eq!("hello", word[0].word)
	}

	const EXAMPLE_WORD: &str = r#"[{"word":"hello","phonetics":[{"audio":"https://api.dictionaryapi.dev/media/pronunciations/en/hello-au.mp3","sourceUrl":"https://commons.wikimedia.org/w/index.php?curid=75797336","license":{"name":"BY-SA 4.0","url":"https://creativecommons.org/licenses/by-sa/4.0"}},{"text":"/həˈləʊ/","audio":"https://api.dictionaryapi.dev/media/pronunciations/en/hello-uk.mp3","sourceUrl":"https://commons.wikimedia.org/w/index.php?curid=9021983","license":{"name":"BY 3.0 US","url":"https://creativecommons.org/licenses/by/3.0/us"}},{"text":"/həˈloʊ/","audio":""}],"meanings":[{"partOfSpeech":"noun","definitions":[{"definition":"\"Hello!\" or an equivalent greeting.","synonyms":[],"antonyms":[]}],"synonyms":["greeting"],"antonyms":[]},{"partOfSpeech":"verb","definitions":[{"definition":"To greet with \"hello\".","synonyms":[],"antonyms":[]}],"synonyms":[],"antonyms":[]},{"partOfSpeech":"interjection","definitions":[{"definition":"A greeting (salutation) said when meeting someone or acknowledging someone’s arrival or presence.","synonyms":[],"antonyms":[],"example":"Hello, everyone."},{"definition":"A greeting used when answering the telephone.","synonyms":[],"antonyms":[],"example":"Hello? How may I help you?"},{"definition":"A call for response if it is not clear if anyone is present or listening, or if a telephone conversation may have been disconnected.","synonyms":[],"antonyms":[],"example":"Hello? Is anyone there?"},{"definition":"Used sarcastically to imply that the person addressed or referred to has done something the speaker or writer considers to be foolish.","synonyms":[],"antonyms":[],"example":"You just tried to start your car with your cell phone. Hello?"},{"definition":"An expression of puzzlement or discovery.","synonyms":[],"antonyms":[],"example":"Hello! What’s going on here?"}],"synonyms":[],"antonyms":["bye","goodbye"]}],"license":{"name":"CC BY-SA 3.0","url":"https://creativecommons.org/licenses/by-sa/3.0"},"sourceUrls":["https://en.wiktionary.org/wiki/hello"]}]"#;
}
