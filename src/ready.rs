use poise::serenity_prelude::Ready;

pub fn ready(ready: &Ready) {
	let total_guilds = ready.guilds.iter().count();
	println!("Bot is ready; running on {total_guilds} server(s).")
}
