use anyhow::Result;
use poise::serenity_prelude::{ClientBuilder, GatewayIntents};
use std::time::Duration;
use ureq::Agent;

mod commands;
mod dictionary;
mod ready;
use commands::*;

struct Data {
	agent: Agent,
}

#[tokio::main]
async fn main() -> Result<()> {
	let key = "LEXIBOT_TOKEN";
	let token = dotenv::var(key)?;
	let intents = GatewayIntents::non_privileged();

	let agent: Agent = ureq::AgentBuilder::new()
		.timeout_read(Duration::from_secs(5))
		.timeout_write(Duration::from_secs(5))
		.build();

	let data = Data { agent: agent };

	let framework = poise::Framework::builder()
		.options(poise::FrameworkOptions {
			commands: vec![define::define(), antonyms::antonyms(), synonyms::synonyms()],
			..Default::default()
		})
		.setup(|ctx, ready, framework| {
			Box::pin(async move {
				ready::ready(ready);
				poise::builtins::register_globally(ctx, &framework.options().commands).await?;
				Ok(data)
			})
		})
		.build();

	let client = ClientBuilder::new(token, intents).framework(framework).await;

	client?.start().await?;

	Ok(())
}

type Context<'a> = poise::Context<'a, Data, Error>;
type Error = Box<dyn std::error::Error + Send + Sync>;
