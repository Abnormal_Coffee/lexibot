use crate::{dictionary::Word, Context, Error};
use anyhow::Result;
use itertools::Itertools;
use poise::{serenity_prelude::CreateEmbed, CreateReply};

#[poise::command(slash_command, prefix_command)]
pub async fn antonyms(
	ctx: Context<'_>,
	#[description = "Word to define"] word: String,
) -> Result<(), Error> {
	{
		let agent = ctx.data().agent.clone();

		let response: String = agent
			.get(&format!("https://api.dictionaryapi.dev/api/v2/entries/en/{word}"))
			.call()?
			.into_string()?;

		let words: Vec<Word> = serde_json::from_str(&response)?;

		let word = &words[0];

		let antonyms: String = word
			.meanings
			.iter()
			.map(|meaning| &meaning.antonyms)
			.flat_map(|f| f)
			.unique()
			.map(|word| format!("- {word}"))
			.join("\n");

		let embed = CreateEmbed::default()
			.title(&word.word)
			.field("Antonyms", antonyms, false)
			.field("Support the free dictionary?", "https://dictionaryapi.dev/", false);

		ctx.send(CreateReply::default().embed(embed)).await?;
	}

	Ok(())
}
