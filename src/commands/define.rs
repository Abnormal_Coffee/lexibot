use crate::{dictionary::Word, Context, Error};
use anyhow::Result;
use poise::{
	serenity_prelude::{futures::future::join_all, CreateAttachment, CreateEmbed, CreateMessage},
	CreateReply,
};

#[poise::command(slash_command, prefix_command)]
pub async fn define(
	ctx: Context<'_>,
	#[description = "Word to define"] word: String,
) -> Result<(), Error> {
	let agent = ctx.data().agent.clone();

	let response: String = agent
		.get(&format!("https://api.dictionaryapi.dev/api/v2/entries/en/{word}"))
		.call()?
		.into_string()?;

	let words: Vec<Word> = serde_json::from_str(&response)?;

	let word = &words[0];

	let meanings = word.meanings_embed();

	let phonetics_paths: Vec<String> =
		word.phonetics.iter().filter_map(|phonetic| phonetic.audio.clone()).collect();

	let phonetics =
		join_all(phonetics_paths.iter().map(|url| CreateAttachment::url(ctx.http(), url))).await;

	let attachments = phonetics.iter().filter_map(|attachment| {
		if let Ok(attachment) = attachment {
			return Some(attachment.clone());
		}
		None
	});

	let embed = CreateEmbed::default().title(&word.word).fields(meanings).field(
		"Support the free dictionary?",
		"https://dictionaryapi.dev/",
		false,
	);

	ctx.send(CreateReply::default().embed(embed)).await?;

	ctx.channel_id().send_files(ctx.http(), attachments, CreateMessage::default()).await?;

	Ok(())
}

impl Word {
	fn meanings_embed(&self) -> Vec<(&String, String, bool)> {
		self.meanings
			.iter()
			.map(|meaning| {
				let definitions_and_examples: String = meaning
					.definitions
					.iter()
					.map(|definition| {
						if let Some(example) = &definition.example {
							format!("\n\n- {}\n> {}", definition.definition, example)
						} else {
							format!("\n\n- {}", definition.definition)
						}
					})
					.collect::<Vec<String>>()
					.join("");
				(&meaning.part_of_speech, definitions_and_examples.clone(), false)
			})
			.collect()
	}
}
